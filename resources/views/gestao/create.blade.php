@extends('sistema.layout.principal')

@section('content_principal')

    <div class="row-offcanvas row-offcanvas-left">
        <div id="sidebar" class="sidebar-offcanvas">
            <div class="col-md-12">
            <h3>WA</h3>
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="/gestao">Empresas</a></li>
            </ul>
            </div>
        </div>
        <div id="main">
            <div id="corpopagina" class="col-md-12">
                <p class="visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
                </p>  
                
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @elseif (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <h1>Empresa</h1>
                <br/><br/>

                <form action="{{url("/gestao/create/salvar")}}" method="POST">
                    <div class="row">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="col-md-6">
                            <label for="nome">Nome:</label>
                            <input type="text" value="{{old('nome')}}" class="form-control" name="nome" id="nome">
                        </div>
                        <div class="col-md-6">
                            <label for="cnpj">CNPJ:</label>    
                            <input type="text" value="{{old('cnpj')}}" class="form-control" name="cnpj" id="cnpj">
                        </div>
                        <div class="col-md-6">
                            <label for="logradouro">Logradouro:</label>    
                            <input type="text" value="{{old('logradouro')}}" class="form-control" name="logradouro" id="logradouro">
                        </div>
                        <div class="col-md-6">
                            <label for="numero">Número:</label>    
                            <input type="text" value="{{old('numero')}}" class="form-control" name="numero" id="numero">
                        </div>
                        <div class="col-md-6">
                            <label for="bairro">Bairro:</label>    
                            <input type="text" value="{{old('bairro')}}" class="form-control" name="bairro" id="bairro">
                        </div>
                        <div class="col-md-6">
                            <label for="cidade">Cidade:</label>    
                            <input type="text" value="{{old('cidade')}}" class="form-control" name="cidade" id="cidade">
                        </div>
                        <div class="col-md-6">
                            <label for="estado">Estado:</label>    
                            <input type="text" value="{{old('estado')}}" class="form-control" name="estado" id="estado">
                        </div>
                        <div class="col-md-6">                
                            <label for="telefone">Telefone:</label>
                            <input type="text" value="{{old('telefone')}}" class="form-control" name="telefone" id="telefone">     
                        </div>
                    </div>
                    <br/>
                    <div class="row col-md-12">
                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                        <a href="/gestao" type="submit" class="btn btn-default">Voltar</a>
                    </div>                    
                </form>

            </div>
        </div>
    </div><!--/row-offcanvas -->

@endsection