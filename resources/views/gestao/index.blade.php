@extends('sistema.layout.principal')

@section('content_principal')

    <div class="row-offcanvas row-offcanvas-left">
        <div id="sidebar" class="sidebar-offcanvas">
            <div class="col-md-12">
            <h3>WA</h3>
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#">Empresas</a></li>
            </ul>
            </div>
        </div>
        <div id="main">
            <div id="corpopagina" class="col-md-12">
                <p class="visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
                </p>  
                
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @elseif (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <h1>Empresas</h1>
                <a href="gestao/create" class="btn btn-success">Novo Cadastro</a>
                <br/><br/>
                <div class="table-responsive datatables-setup">
                    <table id="tabelaEmpresa" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($empresas as $empresa)
                                <tr>
                                    <td><a href="#">{{$empresa->id}}</a></td>
                                    <td><a href="#">{{$empresa->nome}}</a></td>
                                    <td>
                                        <a href="{{url("gestao/vincular/$empresa->id")}}" class="btn btn-primary">
                                            Vincular
                                        </a>
                                        {{ Form::open(['route' => array('gestao.empresa.delete', $empresa->id), 'method' => 'delete', 'class' => 'form-delete', 'style' => 'display: inline-block;']) }}
                                            {{ Form::submit('excluir', ['class' => 'btn btn-danger' ])}}
                                        {{ Form::close()}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Ações</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div><!--/row-offcanvas -->
    <script>
        $(document).ready(function() {
            $('#tabelaEmpresa').DataTable( {
                "language": {
                    "lengthMenu": "Exibir _MENU_ por página",
                    "zeroRecords": "Não foi encontrado registros",
                    "info": "Exibindo página _PAGE_ de _PAGES_",
                    "infoEmpty": "Não foi encontrado registros",
                    "search": "Buscar",
                    "paginate": {
                        "previous": "Anterior ",
                        "next": " Próxima"
                    }
                }
            } );
        });
    </script>

@endsection