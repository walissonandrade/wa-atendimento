@extends('sistema.layout.principal')

@section('content_principal')

    <div class="row-offcanvas row-offcanvas-left">
        <div id="sidebar" class="sidebar-offcanvas">
            <div class="col-md-12">
            <h3>WA</h3>
            <ul class="nav nav-pills nav-stacked">
                <li><a href="/gestao">Empresas</a></li>
                <li class="active"><a href="#">Vincular</a></li>
            </ul>
            </div>
        </div>
        <div id="main">
            <div id="corpopagina" class="col-md-12">
                <p class="visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
                </p>  
                
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @elseif (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <h1>{{$empresa->nome}}</h1>
                <br/><br/>

                <form action="{{url("/gestao/vincular/$empresa->id/store")}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Nome:</label>
                            <input type="text" value="{{old('name')}}" class="form-control" name="name" id="nome">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="text" value="{{old('email')}}" class="form-control" name="email" id="email">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password">Senha:</label>
                            <input type="password" value="{{old('password')}}" class="form-control" name="password" id="cnpj">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password_confirmation">Confirmar Senha:</label>
                            <input type="password" value="{{old('password_confirmation')}}" class="form-control" name="password_confirmation" id="cpf">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                    <a href="/gestao" type="submit" class="btn btn-default">Voltar</a>
                    <br/><br/><br/>                    
                </form>

            </div>
        </div>
    </div><!--/row-offcanvas -->
    <script>
        $(document).ready(function() {

        });
    </script>

@endsection