<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Wa Atendimento</title>

    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Wa Atendimento</a>
                </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a style="cursor:pointer;">Inicio</a></li>
                    <li><a id="sobre" style="cursor:pointer;">Sobre</a></li>
                    <li><a id="contato" style="cursor:pointer;">Contato</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('login') }}">Login <span class="sr-only">(current)</span></a></li>
                <!--<li><a href="{{ route('register') }}">Registrar</a></li> !-->
                </ul>
            </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
        
        @yield('content_site')
    
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
