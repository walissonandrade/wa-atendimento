@extends('site.layout_site')

@section('content_site')
<div class="container">

  <div id="inicio" class="row">
      <div class="col-md-6">
        <div class="thumbnail">
          <img src="{!! asset('/imagens/atendimento.jpeg') !!}" style="height: 336px; width: 100%">
          <div class="caption">
            <p>Eficaz para controle de atendimento em varios cenários, 
                conta com um histórico do atendimento e uma agenda para facilitar
                a vida do cliente.
            </p>
        </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="thumbnail">
          <img src="{!! asset('/imagens/dentista.jpg') !!}" style="height: 336px; width: 100%">
          <div class="caption">
            <p>O cenário de um consultório odontológico é um ótimo exemplo de uso 
                do software Wa Atendimento.
            </p>
          </div>
        </div>
      </div>
  </div>

  <div id="divSobre" class="row">
    <div class="col-md-12">
      <h1>Sobre</h1>
      <h4>
        <p>
          Wa Atendimento é um software totalmente Web desenvolvido utilizando 
          ferramentas tecnologicas de grande relevância no mundo do desenvolvimento.
        </p>
        <p>O software foi desenvolvido por Walisson Andrade.</p>
        <b>Link Linkedin</b>
        <p>https://www.linkedin.com/profile/view?id=361320912&trk=nav_responsive_tab_profile</p>            
      </h3>
    </div>
  </div>
  <br/>

  <div id="divContato" class="row">
    <div class="col-md-12">
      <h1>Contato</h1>
      <h4>
        <p>Desenvolvimento</p>
        <p>Walisson Andrade</p>
        <a><p>walisson.r.andrade@hotmail.com</p></a> 
      </h4>           
    </div>
  </div>

</div>

<script>
    $(document).click(function() {
      // Sobre
      $("#sobre").click(function() {
        $("html, body").animate({
          scrollTop: $("#divSobre").offset().top
        }, 200);
      });
      // Contato
      $("#contato").click(function() {
        $("html, body").animate({
          scrollTop: $("#divContato").offset().top
        }, 200);
      });
    });
</script>

@endsection
