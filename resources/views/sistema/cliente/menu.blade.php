@extends('sistema.layout.principal')

@section('content_principal')

    <div class="row-offcanvas row-offcanvas-left">
        <div id="sidebar" class="sidebar-offcanvas">
            <div class="col-md-12">
                <h3>WA</h3>
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="{{ url('/painel/dashboard')}}">Inicio</a></li>
                    <li><a href="{{url('/painel/empresa/edit')}}">Empresa</a></li>
                    <li><a href="{{url('/painel/perfil/edit')}}">Perfil</a></li>
                    <li><a href="{{url('/painel/usuario')}}">Usuário</a></li>
                    <li><a href="{{ url('/painel/cliente')}}">Cliente</a></li>
                    <li class="active"><a href="{{ url("/painel/cliente/$cliente->id/menu")}}">Menu</a></li>
                    <li><a href="{{url("painel/cliente/$cliente->id/menu/atendimento")}}">Atendimento</a></li>
                    <li><a href="{{url("painel/cliente/$cliente->id/menu/agenda")}}">Agenda</a></li>
                    <li>
                        <a id="sair" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Sair
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </div>
        <div id="main">
            <div id="corpopagina" class="col-md-12">
                <p class="visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
                </p>  
                
                <div class="col-sm-4" style="text-align: center">
                    <a href="{{url("painel/cliente/$cliente->id/menu/atendimento")}}">
                        <h2>Atendimento</h2>
                        <span style="font-size: 80px;" class="glyphicon glyphicon-file" aria-hidden="true"></span>
                    </a>
                </div>
                
                <div class="col-sm-4 col-sm-offset-2" style="text-align: center">
                    <a href="{{url("painel/cliente/$cliente->id/menu/agenda")}}">
                        <h2>Agenda</h2>
                        <span style="font-size: 80px;" class="glyphicon glyphicon-dashboard"></span>
                    </a>
                </div>
                


            </div>
        </div>
    </div><!--/row-offcanvas -->
    

@endsection