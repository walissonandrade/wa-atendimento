@extends('sistema.layout.principal')

@section('content_principal')

    <div class="row-offcanvas row-offcanvas-left">
        <div id="sidebar" class="sidebar-offcanvas">
            <div class="col-md-12">
                <h3>WA</h3>
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="{{ url('/painel/dashboard')}}">Inicio</a></li>
                    <li><a href="{{url('/painel/empresa/edit')}}">Empresa</a></li>
                    <li><a href="{{url('/painel/perfil/edit')}}">Perfil</a></li>
                    <li><a href="{{url('/painel/usuario')}}">Usuário</a></li>
                    <li class="active"><a href="{{ url('/painel/cliente')}}">Cliente</a></li>
                    <li>
                        <a id="sair" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Sair
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </div>
        <div id="main">
            <div id="corpopagina" class="col-md-12">
                <p class="visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
                </p>  
                
                <h1>Cliente</h1>

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @elseif (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form action="/painel/cliente/store" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nome">Nome:</label>
                            <input type="text" value="{{old('nome')}}" class="form-control" name="nome" id="nome">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="text" value="{{old('email')}}" class="form-control" name="email" id="email">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cnpj">CNPJ:</label>
                            <input type="text" value="{{old('cnpj')}}" class="form-control" name="cnpj" id="cnpj">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cpf">CPF:</label>
                            <input type="text" value="{{old('cpf')}}" class="form-control" name="cpf" id="cpf">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="telefone">Telefone:</label>
                            <input type="text" value="{{old('telefone')}}" class="form-control" name="telefone" id="telefone">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                    <a href="/painel/cliente" type="submit" class="btn btn-default">Voltar</a>
                    <br/><br/><br/>                    
                </form>
                
            </div>
        </div>
    </div><!--/row-offcanvas -->
    <script src="{{ asset('js/jqueryMask.js') }}"></script>  
    <script>
        $(document).ready(function(){
            $('#cnpj').mask('00.000.000/0000-00');
            $('#cpf').mask('000.000.000-00');
            $('#telefone').mask('00-0000-0000');
        });
    </script>
@endsection