@extends('sistema.layout.principal')

@section('content_principal')

    <div class="row-offcanvas row-offcanvas-left">
        <div id="sidebar" class="sidebar-offcanvas">
            <div class="col-md-12">
            <h3>WA</h3>
            <ul class="nav nav-pills nav-stacked">
                <li><a href="{{ url('/painel/dashboard')}}">Inicio</a></li>
                <li><a href="{{url('/painel/empresa/edit')}}">Empresa</a></li>
                <li><a href="{{url('/painel/perfil/edit')}}">Perfil</a></li>
                <li><a href="{{url('/painel/usuario')}}">Usuário</a></li>
                <li><a href="{{ url('/painel/cliente')}}">Cliente</a></li>
                <li><a href="{{ url("/painel/cliente/$cliente->id/menu")}}">Menu</a></li>
                <li class="active"><a href="{{url("painel/cliente/$cliente->id/menu/atendimento")}}">Atendimento</a></li>
                <li><a href="{{url("painel/cliente/$cliente->id/menu/agenda")}}">Agenda</a></li>
                <li>
                    <a id="sair" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        Sair
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
            </div>
        </div>
        <div id="main">
            <div id="corpopagina" class="col-md-12">
                <p class="visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
                </p>  
                
                <h1>Atendimento</h1>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @elseif (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form action="{{url("/painel/cliente/$cliente->id/menu/atendimento/store")}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="titulo">Título:</label>
                            <input type="text" value="{{old('titulo')}}" class="form-control" name="titulo" id="titulo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="data">Data:</label>
                            <input type="text" value="{{old('data')}}" class="form-control" name="data" id="data">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="descricao">Descricao:</label>
                            <textarea rows="5" value="{{old('descricao')}}" class="form-control" name="descricao" id="descricao"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                    <a href="{{url("/painel/cliente/$cliente->id/menu/atendimento")}}" type="submit" class="btn btn-default">Voltar</a>
                    <br/><br/><br/>                    
                </form>

            </div>
        </div>
    </div><!--/row-offcanvas -->
    <script src="{{ asset('js/jqueryMask.js') }}"></script>  
    <script>
        $(document).ready(function(){
            $('#data').mask('00/00/0000');
        });
    </script>

@endsection