@extends('sistema.layout.principal')

@section('content_principal')

    <div class="row-offcanvas row-offcanvas-left">
        <div id="sidebar" class="sidebar-offcanvas">
            <div class="col-md-12">
                <h3>WA</h3>
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="{{ url('/painel/dashboard')}}">Inicio</a></li>
                    <li><a href="{{url('/painel/empresa/edit')}}">Empresa</a></li>
                    <li class="active"><a href="{{url('/painel/perfil/edit')}}">Perfil</a></li>
                    <li><a href="{{url('/painel/usuario')}}">Usuário</a></li>
                    <li><a href="{{ url('/painel/cliente')}}">Cliente</a></li>
                    <li>
                            <a id="sair" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Sair
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                </ul>
            </div>
        </div>
        <div id="main">
            <div id="corpopagina" class="col-md-12">
                <p class="visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
                </p>
                  
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @elseif (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                
                <h1>Perfil</h1>

                {{ Form::open(['route' => array('perfil.updatereset'), 'method' => 'PATCH', 
                    'class' => 'form-group']) }}
                    <div class="col-md-7">
                        <label for="password">Senha:</label>
                        {{ Form::password('password', ['class' => 'form-control', 'id' => 'password']) }}
                    </div>
                    <div class="col-md-7">
                        <label for="confirmation">Confirmar Senha:</label>    
                        {{ Form::password('confirmation', ['class' => 'form-control', 'id' => 'confirmation']) }}
                    </div>
                    <br/><br/>
                    <div class="col-md-12">
                        {{ Form::submit('Editar', ['class' => 'btn btn-primary', 'style' => 'margin-top: 8px' ])}}
                        <a href="{{url('painel/perfil/edit')}}" class="btn btn-default" style="margin-top: 8px">Voltar</a>
                    </div>
                {{ Form::close()}}


            </div>
        </div>
    </div><!--/row-offcanvas -->

@endsection