<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- As 3 meta tags acima *devem* vir em primeiro lugar dentro do `head`; qualquer outro conteúdo deve vir *após* essas tags -->
    <title>WA Atendimento</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.mim.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sistema.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.css') }}" rel="stylesheet">    
    <!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
    <!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/datatables.js') }}"></script>  
    <script src="{{ asset('js/bootstrap.mim.js') }}"></script>
    <script src="{{ asset('js/sistema.js') }}"></script>
  </head>
  <body>

    <div id="barraSuperior">
      <div class="col-md-3 col-xs-6">Wa Atendimento</div>
      <div class="col-md-3 col-xs-6 col-md-offset-6">
          <div id="login" class="dropdown">
              <a href="#" data-toggle="dropdown" class="dropdown-toggle">{{ Auth::user()->name }} <b class="caret"></b></a>
              <ul class="dropdown-menu dropdown-menu-right">
                  <li>
                    <a id="sair" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      Sair
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                  </li>
              </ul>
          </div>
      </div>
    </div>

    @yield('content_principal')
    

    <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
    <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
  </body>
</html>