@extends('sistema.layout.principal')

@section('content_principal')

<div class="row-offcanvas row-offcanvas-left">
    <div id="sidebar" class="sidebar-offcanvas">
        <div class="col-md-12">
          <h3>WA</h3>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="{{ url('/painel/dashboard')}}">Inicio</a></li>
            <li class="active"><a href="{{url('/painel/empresa/edit')}}">Empresa</a></li>
            <li><a href="{{url('/painel/perfil/edit')}}">Perfil</a></li>
            <li><a href="{{url('/painel/usuario')}}">Usuário</a></li>
            <li><a href="{{ url('/painel/cliente')}}">Cliente</a></li>
            <li>
                    <a id="sair" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        Sair
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
          </ul>
        </div>
    </div>
    <div id="main">
        <div id="corpopagina" class="col-md-12">
            <p class="visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
            </p>  

            <h1>{{$empresa->nome}}</h1>
            
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @elseif (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            {{ Form::open(['route' => array('empresa.update', $empresa->id), 'method' => 'PATCH', 
                   'class' => 'form-group']) }}
                <div class="col-md-6">
                    <label for="nome">Nome:</label>
                    {!! Form::text('nome', $empresa->nome, ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-6">
                    <label for="cnpj">CNPJ:</label>    
                    {!! Form::text('cnpj', $empresa->cnpj , ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-6">
                    <label for="logradouro">Logradouro:</label>    
                    {!! Form::text('logradouro', $empresa->logradouro , ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-6">
                    <label for="numero">Número:</label>    
                    {!! Form::text('numero', $empresa->numero , ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-6">
                    <label for="bairro">Bairro:</label>    
                    {!! Form::text('bairro', $empresa->bairro , ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-6">
                    <label for="cidade">Cidade:</label>    
                    {!! Form::text('cidade', $empresa->cidade , ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-6">
                    <label for="estado">Estado:</label>    
                    {!! Form::text('estado', $empresa->estado , ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-6">                
                    <label for="telefone">Telefone:</label>    
                    {!! Form::text('telefone', $empresa->telefone , ['class' => 'form-control']) !!}
                </div>
                <br/><br/>
                <div class="col-md-12">
                    {{ Form::submit('Editar', ['class' => 'btn btn-primary', 'style' => 'margin-top: 8px' ])}}
                    <a href="{{url('painel/dashboard')}}" class="btn btn-default" style="margin-top: 8px">Voltar</a>
                </div>
            {{ Form::close()}}

        </div>
    </div>
  </div><!--/row-offcanvas -->
    
@endsection