@extends('sistema.layout.principal')

@section('content_principal')

    <div class="row-offcanvas row-offcanvas-left">
        <div id="sidebar" class="sidebar-offcanvas">
            <div class="col-md-12">
            <h3>WA</h3>
            <ul class="nav nav-pills nav-stacked">
                <li><a href="{{ url('/painel/dashboard')}}">Inicio</a></li>
                <li><a href="{{url('/painel/empresa/edit')}}">Empresa</a></li>
                <li><a href="{{url('/painel/perfil/edit')}}">Perfil</a></li>
                <li><a href="{{url('/painel/usuario')}}">Usuário</a></li>
                <li><a href="{{ url('/painel/cliente')}}">Cliente</a></li>
                <li><a href="{{ url("/painel/cliente/$cliente->id/menu")}}">Menu</a></li>
                <li><a href="{{url("painel/cliente/$cliente->id/menu/atendimento")}}">Atendimento</a></li>
                <li class="active"><a href="{{url("painel/cliente/$cliente->id/menu/agenda")}}">Agenda</a></li>
                <li>
                    <a id="sair" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        Sair
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
            </div>
        </div>
        <div id="main">
            <div id="corpopagina" class="col-md-12">
                <p class="visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
                </p>  
                
                <h1>Agenda</h1>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @elseif (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif


                {{ Form::open(['route' => array('agenda.update', $cliente->id, $agendamento->id), 'method' => 'PATCH', 
                    'class' => 'form-group']) }}
                    <div class="col-md-4">
                        <label for="titulo">Título:</label>
                        {!! Form::text('titulo', $agendamento->titulo, ['class' => 'form-control', 'id' => 'titulo']) !!}
                    </div>
                    <div class="col-md-4">
                        <label for="data">Data:</label>    
                        {!! Form::text('data', \Carbon\Carbon::parse($agendamento->data)->format('d/m/Y') , ['class' => 'form-control', 'id' => 'data']) !!}
                    </div>
                    <div class="col-md-4">
                        <label for="hora">Hora:</label>    
                        {!! Form::text('hora', \Carbon\Carbon::parse($agendamento->hora)->format('h:i') , ['class' => 'form-control', 'id' => 'hora']) !!}
                    </div>
                    <div class="col-md-12">
                        <label for="observacao">Observação:</label>    
                        {!! Form::textarea('observacao', $agendamento->observacao , ['class' => 'form-control', 'id' => 'observacao']) !!}
                    </div>
                    <br/><br/>
                    <div class="col-md-12">
                        {{ Form::submit('Editar', ['class' => 'btn btn-primary', 'style' => 'margin-top: 8px' ])}}
                        <a href="{{url("painel/cliente/$cliente->id/menu/agenda")}}" class="btn btn-default" style="margin-top: 8px">Voltar</a>
                    </div>
                {{ Form::close()}}

            </div>
        </div>
    </div><!--/row-offcanvas -->
    <script src="{{ asset('js/jqueryMask.js') }}"></script>  
    <script>
        $(document).ready(function(){
            $('#data').mask('00/00/0000');
            $('#hora').mask('00:00');
        });
    </script>
@endsection