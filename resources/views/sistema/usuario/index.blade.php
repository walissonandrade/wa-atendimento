@extends('sistema.layout.principal')

@section('content_principal')

    <div class="row-offcanvas row-offcanvas-left">
        <div id="sidebar" class="sidebar-offcanvas">
            <div class="col-md-12">
                <h3>WA</h3>
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="{{ url('/painel/dashboard')}}">Inicio</a></li>
                    <li><a href="{{url('/painel/empresa/edit')}}">Empresa</a></li>
                    <li><a href="{{url('/painel/perfil/edit')}}">Perfil</a></li>
                    <li class="active"><a href="{{url('/painel/usuario')}}">Usuário</a></li>
                    <li><a href="{{ url('/painel/cliente')}}">Cliente</a></li>
                    <li>
                            <a id="sair" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Sair
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                </ul>
            </div>
        </div>
        <div id="main">
            <div id="corpopagina" class="col-md-12">
                <p class="visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
                </p>
                  
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @elseif (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                
                <h1>Usuários</h1>
                <a href="{{url("painel/usuario/create")}}" class="btn btn-success">Novo Cadastro</a>
                <br/><br/>
                <div class="table-responsive datatables-setup">
                    <table id="tabelaUsuario" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>E-mail</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $usuario)
                                <tr>
                                    <td><a href="#">{{$usuario->name}}</a></td>
                                    <td><a href="#">{{$usuario->email}}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div><!--/row-offcanvas -->
    
    <script>
        $(document).ready(function() {
            $('#tabelaUsuario').DataTable( {
                "language": {
                    "lengthMenu": "Exibir _MENU_ por página",
                    "zeroRecords": "Não foi encontrado registros",
                    "info": "Exibindo página _PAGE_ de _PAGES_",
                    "infoEmpty": "Não foi encontrado registros",
                    "search": "Buscar",
                    "paginate": {
                        "previous": "Anterior ",
                        "next": " Próxima"
                    }
                }
            } );
        });
    </script>

@endsection