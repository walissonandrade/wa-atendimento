
window.Vue = require('vue');

require('./bootstrap');

import axios from 'axios';


import fullCalendar from 'fullcalendar'


const app = new Vue({
    el: '#calendar',
    data: {
        agenda: []
    },


    mounted() {
        this.getAgenda();
    },


    methods: {
        getAgenda(){
            axios.get(`/painel/dashboard/show`)
        .then(response => {
            this.agenda = response.data.agendas

            var auxAgenda = [];
            for(var i = 0; i < this.agenda.length; i++) {
                auxAgenda.push( {
                    title: this.agenda[i].titulo,
                    start: this.agenda[i].data + 'T' + this.agenda[i].hora
                })}

            $('#calendar').fullCalendar({
                header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
                },
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                
                events: auxAgenda,
                timeFormat: 'H(:mm)',
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                buttonText: {
                    today: "Hoje",
                    month: "Mês",
                    week: "Semana",
                    day: "Dia"
                }
            });
        })
        .catch(e => {
          this.errors.push(e)
        })
        }
    }


});