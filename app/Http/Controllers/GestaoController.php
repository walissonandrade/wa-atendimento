<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;
use App\Empresa;
use App\User;

class GestaoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){

        $empresas = Empresa::all()->sortBy('nome');
        return view('gestao.index', compact('empresas'));
    }

    public function vincular($id){

        $empresa = Empresa::find($id);
        return view('gestao.vincular', compact('empresa'));
    }

    public function store(Request $request, $id){

        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
            'name' => 'required|max:100',
            'email' => 'required|max:100'],
            ['password.required'=>'A senha é obrigatória',
                'password.confirmed'=>'Senha divergente ',
                'password.min'=>'Necessário mínimo de 6 caracteres na senha ',
                'name.required' => 'Nome é obrigatório',
                'name.max' => 'O tamanho maxímo é de 100 caracteres pra nome',
                'email.required' => 'Email é obrigatório',
                'email.max' => 'O tamanho maxímo é de 100 caracteres pro e-mail']
        );
        $empresa = Empresa::find($id);
        $usuario = new User;
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);
        $usuario->empresa_id = $empresa->id;
        try{
            $usuario->save();
        } catch(QueryException $e) {
            return redirect()->back()
                ->withErrors('Erro ao comunicar com servidor!');
        }

        return back()->with('status', 'E-mail Vinculado!');
    }

    public function create(){

        return view('gestao.create');
    }

    public function salvar(Request $request){
        
        $this->validate($request, [
            'nome' => 'required|max:100',
            'cnpj' => 'required|max:18',
            'logradouro' => 'max:18',
            'numero' => 'max:8',
            'bairro' => 'max:100',
            'cidade' => 'max:100',
            'estado' => 'max:80',
            'telefone' => 'required|max:13'], 
            [
            'nome.required' => 'O nome é obrigatório',
            'nome.max' => 'O tamanho máximo para nome é 100',
            'cnpj.required' => 'O CNPJ é obrigatório',
            'cnpj.max' => 'O tamanho máximo para CNPJ é 18',
            'logradouro.max' => 'O tamanho máximo para nome é 100',
            'numero.max' => 'O tamanho máximo para número é 8',
            'bairro.max' => 'O tamanho máximo para bairro é 100',
            'cidade.max' => 'O tamanho máximo para cidade é 100',
            'estado.max' => 'O tamanho máximo para estado é 80',
            'telefone.required' => 'O telefone é obrigatório',
            'telefone.max' => 'O tamanho máximo para telefone é 13'
            ]);

        $empresa = new Empresa();
        $empresa->nome = $request->nome;
        $empresa->cnpj = $request->cnpj;
        $empresa->logradouro = $request->logradouro;
        $empresa->numero = $request->numero;
        $empresa->bairro = $request->bairro;
        $empresa->cidade = $request->cidade;
        $empresa->estado = $request->estado;
        $empresa->telefone = $request->telefone;
        
        try{
            $empresa->save();
        } catch(QueryException $e) {
            return redirect()->route('empresa.edit', compact('empresa'))
                ->withErrors('Erro ao comunicar com servidor!');
        }

        return bAck()->with('status', 'Empresa Cadastrada!');
    }

    public function delete($id){

        Empresa::find($id)->delete();
        return back()->with('status', 'Empresa Deletada!');
    }
}
