<?php

namespace App\Http\Controllers\Sistema;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;
use App\Empresa;

class EmpresaController extends Controller
{
    public function edit(){

        $empresa = Empresa::find(auth()->user()->empresa_id);

        return view('sistema.empresa.edit', compact('empresa'));
    }

    public function update(Request $request, $id){
        
        $this->validate($request, [
            'nome' => 'required|max:100',
            'cnpj' => 'required|max:18',
            'logradouro' => 'max:18',
            'numero' => 'max:8',
            'bairro' => 'max:100',
            'cidade' => 'max:100',
            'estado' => 'max:80',
            'telefone' => 'required|max:13'], 
            [
            'nome.required' => 'O nome é obrigatório',
            'nome.max' => 'O tamanho máximo para nome é 100',
            'cnpj.required' => 'O CNPJ é obrigatório',
            'cnpj.max' => 'O tamanho máximo para CNPJ é 18',
            'logradouro.max' => 'O tamanho máximo para nome é 100',
            'numero.max' => 'O tamanho máximo para número é 8',
            'bairro.max' => 'O tamanho máximo para bairro é 100',
            'cidade.max' => 'O tamanho máximo para cidade é 100',
            'estado.max' => 'O tamanho máximo para estado é 80',
            'telefone.required' => 'O telefone é obrigatório',
            'telefone.max' => 'O tamanho máximo para telefone é 13'
            ]);

        $empresa = Empresa::find($id);
        $empresa->nome = $request->nome;
        $empresa->cnpj = $request->cnpj;
        $empresa->logradouro = $request->logradouro;
        $empresa->numero = $request->numero;
        $empresa->bairro = $request->bairro;
        $empresa->cidade = $request->cidade;
        $empresa->estado = $request->estado;
        $empresa->telefone = $request->telefone;
        
        try{
            $empresa->save();
        } catch(QueryException $e) {
            return redirect()->route('empresa.edit', compact('empresa'))
                ->withErrors('Erro ao comunicar com servidor!');
        }

        return redirect()->route('empresa.edit', compact('empresa'))
            ->with('status', 'Empresa Atualizada!');
    }
}
