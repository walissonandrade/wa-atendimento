<?php

namespace App\Http\Controllers\Sistema;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;
use App\Cliente;
use App\Empresa;
class ClienteController extends Controller
{
    public function index(){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        
        $clientes = Cliente::all()->sortBy('nome')->where('empresa_id', $empresa->id );
        return view('sistema.cliente.index', compact('clientes', 'empresa'));
    }

    public function create(){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        return view('sistema.cliente.create', compact('empresa'));
    }

    public function store(Request $request){
        
        $this->validate($request, [
            'nome' => 'required|max:100',
            'telefone' => 'required|max:12',
            'email' => 'max:100',
            'cnpj' => 'max:18',
            'cpf' => 'max:14'],
            [
            'nome.required' => 'O nome é obrigatório',
            'nome.max' => 'O tamanho máximo para nome é 100',
            'telefone.required' => 'O telefone é obrigatório',
            'telefone.max' => 'O tamanho máximo para telefone é 12',
            'email,max' => 'O tamanho máximo para E-mail é 100',
            'cnpj.max' => 'O tamanho máximo para CNPJ é 18',
            'cpf.max' => 'O tamanho máximo para CPF é 14'
            ]);

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = new Cliente;
        $cliente->empresa_id = $empresa->id;
        $cliente->nome = $request->nome;
        $cliente->telefone = $request->telefone;
        $cliente->email = $request->email;
        $cliente->cnpj = $request->cnpj;
        $cliente->cpf = $request->cpf;

        try{
            $cliente->save();
        } catch(QueryException $e) {
            return redirect()->route('clientes', compact('empresa'))
                ->withErrors('Erro ao comunicar com servidor!');
        }

            return redirect()->route('clientes', compact('empresa'))
                ->with('status', 'Cliente cadastrado');
    }

    public function edit($id){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($id);

        return view('sistema.cliente.edit', compact('cliente', 'empresa'));
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            'nome' => 'required|max:100',
            'telefone' => 'required|max:12',
            'email' => 'max:100',
            'cnpj' => 'max:18',
            'cpf' => 'max:14'],
            [
            'nome.required' => 'O nome é obrigatório',
            'nome.max' => 'O tamanho máximo para nome é 100',
            'telefone.required' => 'O telefone é obrigatório',
            'telefone.max' => 'O tamanho máximo para telefone é 12',
            'email,max' => 'O tamanho máximo para E-mail é 100',
            'cnpj.max' => 'O tamanho máximo para CNPJ é 18',
            'cpf.max' => 'O tamanho máximo para CPF é 14'
            ]);

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($id);
        $cliente->nome = $request->nome;
        $cliente->telefone = $request->telefone;
        $cliente->email = $request->email;
        $cliente->cnpj = $request->cnpj;
        $cliente->cpf = $request->cpf;
        
        try{
            $cliente->save();
        } catch(QueryException $e) {
            return redirect()->route('clientes.edit', compact('cliente','empresa'))
                ->withErrors('Erro ao comunicar com servidor!');
        }


        return redirect()->route('clientes.edit', compact('cliente','empresa'))
            ->with('status', 'Cliente Atualizado');
    }

    public function menu($id){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        
        $cliente = Cliente::find($id);
        return view('sistema.cliente.menu', compact('cliente', 'empresa'));
    }

    public function delete($id){

        Cliente::find($id)->delete();
        return back()->with('status', 'Cliente Deletado!');
    }
}
