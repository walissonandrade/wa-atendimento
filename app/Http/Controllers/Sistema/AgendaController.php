<?php

namespace App\Http\Controllers\Sistema;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;
use App\Empresa;
use App\Cliente;
use App\Agenda;
use App\User;
use Gate;

class AgendaController extends Controller
{

    public function index($id){
        
        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($id);
        $agendamentos = Agenda::all()->sortByDesc('data')->where('cliente_id', $cliente->id);
        return view('sistema.agenda.index', compact('cliente','empresa', 'agendamentos'));
    }

    public function create($id){
        
        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($id);
        return view('sistema.agenda.create', compact('cliente','empresa'));
    }

    public function store($id, Request $request){

        $this->validate($request, [
            'titulo' => 'required|max:100',
            'data' => 'date_format:"d/m/Y"|required|max:10|',
            'observacao' => 'required|max:100', 
            'hora' => 'date_format:"H:i"|required'],[
                'titulo.required' => 'O título é obrigatório',
                'titulo.max' => 'O número máximo de caracteres é 100 para o título',
                'data.required' => 'A data inicial é obrigatória',
                'data.max' => 'O número máximo de caracteres é 10 para a data',
                'data.date_format' => 'O formato da data é DD/MM/AAAA',
                'observacao.required' => 'A observação é obrigatória',
                'observacao.max' => 'O número máximo de caracteres é 100 para a observação',
                'hora.date_format' => 'O formato para hora é HH:MM',
                'hora.required' => 'O campo hora é obrigatório',
            ]
        );
        $campoData =  \Carbon\Carbon::createFromFormat('d/m/Y', $request->data);
        $cliente = Cliente::find($id);
        $agenda = new Agenda;
        $agenda->cliente_id = $cliente->id;
        $agenda->titulo = $request->titulo;
        $agenda->observacao = $request->observacao;
        $agenda->data = $campoData->format('y-m-d');
        $agenda->hora = $request->hora;
        
    
        $agenda->save();
      

        return redirect()->route('agenda.create', compact('cliente','empresa'))->with('status', 'Agendamento Cadastrado!');
    }

    public function edit($cliente_id, $id){
        
        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($cliente_id);
        $agendamento = Agenda::find($id);

        if($agendamento->cliente_id != $cliente->id){
            return back();
        }

        return view('sistema.agenda.edit', compact('cliente','empresa', 'agendamento'));
    }

    public function update($cliente_id, $id, Request $request){

        $this->validate($request, [
            'titulo' => 'required|max:100',
            'data' => 'date_format:"d/m/Y"|required|max:10|',
            'observacao' => 'required|max:100', 
            'hora' => 'date_format:"H:i"|required'],[
                'titulo.required' => 'O título é obrigatório',
                'titulo.max' => 'O número máximo de caracteres é 100 para o título',
                'data.required' => 'A data inicial é obrigatória',
                'data.max' => 'O número máximo de caracteres é 10 para a data',
                'data.date_format' => 'O formato da data é DD/MM/AAAA',
                'observacao.required' => 'A observação é obrigatória',
                'observacao.max' => 'O número máximo de caracteres é 100 para a observação',
                'hora.date_format' => 'O formato para hora é HH:MM',
                'hora.required' => 'O campo hora é obrigatório',
            ]
        );
        $campoData =  \Carbon\Carbon::createFromFormat('d/m/Y', $request->data);
        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($cliente_id);
        $agenda = Agenda::find($id);
        $agenda->titulo = $request->titulo;
        $agenda->observacao = $request->observacao;
        $agenda->data = $campoData->format('Y-m-d');
        $agenda->hora = $request->hora;

        
        try{
            $agenda->save();
        } catch(QueryException $e) {
            return redirect()->route('agenda.edit', compact('cliente', 'agenda','empresa'))
                ->withErrors('Erro ao comunicar com servidor!');
        }

        return redirect()->route('agenda.edit', compact('cliente', 'agenda','empresa'))
                ->with('status', 'Agendamento Atualizado!');
    }

    public function delete($cliente_id, $id){

        Agenda::find($id)->delete();

        return back()->with('status', 'Agendamento Deletado!');
    }
}
