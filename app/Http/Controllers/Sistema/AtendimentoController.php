<?php

namespace App\Http\Controllers\Sistema;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;
use App\Cliente;
use App\Empresa;
use App\Atendimento;

class AtendimentoController extends Controller
{
    public function index($id){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($id);
        $atendimentos = Atendimento::All()->sortBy('titulo')->where('cliente_id', $cliente->id);
        return view('sistema.atendimento.index', compact('cliente','atendimentos','empresa'));
    }

    public function create($id){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($id);
        return view('sistema.atendimento.create', compact('cliente','empresa'));
    }

    public function store($id, Request $request){

        $this->validate($request, [
            'titulo' => 'required|max:100',
            'descricao' => 'required|max:300',
            'data' => 'date_format:"d/m/Y"|required|max:10|'],[
                'titulo.required' => 'O título é obrigatório',
                'titulo.max' => 'O número máximo de caracteres é 100 para o título',
                'descricao.required' => 'A descrição é obrigatória',
                'descricao.max' => 'O número máximo de caracteres é 100 para a descrição',
                'data.date_format' => 'O formato da data é DD/MM/AAAA',
                'data.required' => 'A data é obrigatória'
            ]
        );

        $campoData =  \Carbon\Carbon::createFromFormat('d/m/Y', $request->data);
        $cliente = Cliente::find($id);
        $atendimento = new Atendimento;
        $atendimento->cliente_id = $cliente->id;
        $atendimento->titulo = $request->titulo;
        $atendimento->data = $campoData->format('Y-m-d');
        $atendimento->descricao = $request->descricao;
        
        try{
            $atendimento->save();
        } catch(QueryException $e) {
            return redirect()->route('atendimento.create', compact('cliente','empresa'))
                ->withErrors('Erro ao comunicar com servidor!');
        }
        
        return redirect()->route('atendimento.create', compact('cliente','empresa'))
            ->with('status', 'Atendimento Cadastrado!');
    }

    public function edit($cliente_id, $id){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($cliente_id);
        $atendimento = Atendimento::find($id);

        if($atendimento->cliente_id != $cliente->id){
            return back();
        }

        return view('sistema.atendimento.edit', compact('atendimento','cliente','empresa'));
    }

    public function update($cliente_id, $id, Request $request){

        $this->validate($request, [
            'titulo' => 'required|max:100',
            'descricao' => 'required|max:300',
            'data' => 'date_format:"d/m/Y"|required|max:10|'],[
                'titulo.required' => 'O título é obrigatório',
                'titulo.max' => 'O número máximo de caracteres é 100 para o título',
                'descricao.required' => 'A descrição é obrigatória',
                'descricao.max' => 'O número máximo de caracteres é 100 para a descrição',
                'data.date_format' => 'O formato da data é DD/MM/AAAA',
                'data.required' => 'A data é obrigatória'
            ]);

        $campoData =  \Carbon\Carbon::createFromFormat('d/m/Y', $request->data);
        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($cliente_id);
        $atendimento = Atendimento::find($id);
        $atendimento->titulo = $request->titulo;
        $atendimento->data = $campoData->format('Y-m-d');
        $atendimento->descricao = $request->descricao;

        try{
            $atendimento->save();
        } catch(QueryException $e) {
            return redirect()->route('atendimento.edit', compact('cliente', 'atendimento','empresa'))
                ->withErrors('Erro ao comunicar com servidor!');
        }

        return redirect()->route('atendimento.edit', compact('cliente', 'atendimento','empresa'))
            ->with('status', 'Atendimento Atualizado!');
    }

    public function delete($cliente_id, $id){

        Atendimento::find($id)->delete();

        return back()->with('status', 'Atendimento Deletado!');
    }
}
