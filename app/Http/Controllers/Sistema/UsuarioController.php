<?php

namespace App\Http\Controllers\Sistema;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;
use App\Empresa;
use App\User;

class UsuarioController extends Controller
{

    public function index(){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $usuarios = User::all()->where('empresa_id', $empresa->id);

        return view('sistema.usuario.index', compact('usuarios','empresa'));
    }


    public function create(){
        
        $empresa = Empresa::find(auth()->user()->empresa_id);

        return view('sistema.usuario.create', compact('empresa'));
    }
    
    public function store(Request $request){
        
        
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
            'name' => 'required|max:100',
            'email' => 'required|max:100'],
            ['password.required'=>'A senha é obrigatória',
                'password.confirmed'=>'Senha divergente ',
                'password.min'=>'Necessário mínimo de 6 caracteres na senha ',
                'name.required' => 'Nome é obrigatório',
                'name.max' => 'O tamanho maxímo é de 100 caracteres pra nome',
                'email.required' => 'Email é obrigatório',
                'email.max' => 'O tamanho maxímo é de 100 caracteres pro e-mail']
        );
        $empresa = Empresa::find(auth()->user()->empresa_id);
        $usuario = new User;
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);
        $usuario->empresa_id = $empresa->id;
        
        try{
            $usuario->save();
        } catch(QueryException $e) {
            return redirect()->back()
                ->withErrors('Erro ao comunicar com servidor!');
        }
        
        return redirect()->back()->with('status', 'Usuário cadastrado com sucesso');
    }

}
