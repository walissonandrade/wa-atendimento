<?php

namespace App\Http\Controllers\Sistema;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use App\Empresa;
use App\User;
use App\Cliente;
use App\Agenda;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $cliente = Cliente::find($empresa->id);
        $events = [];
        if ($cliente != null) {
            $data = Agenda::all()->where('cliente_id', $cliente->id);
        } else {
            $data = new Agenda();
        }
        
        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->titulo,
                    false,
                    new \DateTime($value->data.' '.$value->hora),
                    new \DateTime($value->data.' '.$value->hora),
                    null,
                    // Add color and link on event
                 [
                     'color' => '#428bca',
                ]
                );
            }
            
        }
        $calendar = Calendar::addEvents($events);
     //   dd($calendar);

        return view('sistema.dashboard.index', compact('calendar', 'empresa', 'data'));
    }


    public function show(){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $clientes = Cliente::all()->where('empresa_id', $empresa->id);

        if (!$clientes->isEmpty()) {
            foreach($clientes as $cliente){
                $auxAgendas = Agenda::all()->where('cliente_id', $cliente->id);
                foreach($auxAgendas as $auxAgenda){
                    
                    $agendas[] = $auxAgenda;
                }
            }
        } else {
            $agendas = new Agenda();
        }

        $response['agendas'] = $agendas;
        
        return response()->json($response, 200);
    }
}
