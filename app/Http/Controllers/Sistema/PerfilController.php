<?php

namespace App\Http\Controllers\Sistema;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;
use App\Empresa;
use App\User;

class PerfilController extends Controller
{

    public function edit(){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $usuario = auth()->user();

        return view('sistema.perfil.edit', compact('usuario','empresa'));
    }

    public function update(Request $request){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $usuario = auth()->user();
        $usuario->name = $request->name;
        $usuario->email = $request->email;        
        $this->validate($request, [
            'name' => 'required|max:100',
            'email' => 'required|max:100'],
            ['name.required' => 'Nome é obrigatório',
             'email.required' => 'E-mail é obrigatório']
        );

        try{
            $usuario->save();
        } catch(QueryException $e) {
            return redirect()->back()
                ->withErrors('Erro ao comunicar com servidor!');
        }

        return redirect()->back()->with('status', 'Perfil Atualizado');
    }

    public function reset(){

        $empresa = Empresa::find(auth()->user()->empresa_id);
        $usuario = auth()->user();

        return view('sistema.perfil.reset', compact('usuario','empresa'));
    }

    public function updatereset(Request $request){
        $this->validate($request, [
            'password' => 'required|min:6',
            'confirmation' => 'required|min:6'],
            ['password.required'=>'A senha é obrigatória',
             'password.min'=>'Necessário mínimo de 6 caracteres na senha ',
             'confirmation.required'=>'A confirmação da senha é obrigatória',
             'confirmation.min'=>'Necessário mínimo de 6 caracteres na confirmação da senha']
        );

        if($request->password != $request->confirmation){
            // lançar mensagem erro
            return redirect()->back()
                ->withErrors('A senha está divergente!');;
        }
        else{
            $empresa = Empresa::find(auth()->user()->empresa_id);
            $usuario = auth()->user();
            $usuario->password = bcrypt($request->password);
            
            try{
                $usuario->save();
            } catch(QueryException $e) {
                return redirect()->back()
                    ->withErrors('Erro ao comunicar com servidor!');
            }        
        }

        return redirect()->back()->with('status', 'Senha Atualizada');
    }

}
