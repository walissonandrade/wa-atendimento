<?php

namespace App\Policies;

use App\User;
use App\Cliente;
use App\Atendimento;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmpresaPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function controlePorEmpresa(User $user, Cliente $cliente){
        
        return $user->empresa_id == $cliente->empresa_id;
    }

}
