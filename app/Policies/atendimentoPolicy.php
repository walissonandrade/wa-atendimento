<?php

namespace App\Policies;

use App\User;
use App\Cliente;
use App\Atendimento;
use Illuminate\Auth\Access\HandlesAuthorization;

class AtendimentoPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function controleAtendimento(User $user, Atendimento $atendimento){
        
        $cliente = Cliente::find($atendimento->cliente_id); 
        return $user->empresa_id == $cliente->empresa_id;
    }
}
