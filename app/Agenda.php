<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    public function agenda (){

        return $this->belongsTo(Agenda::class);
    }

    protected $fillable = ['cliente_id','titulo','data', 'hora', 'observacao'];
}
