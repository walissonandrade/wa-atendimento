<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
return view('site.index');
});

Auth::routes();

// Rota Site
Route::get('/home', 'HomeController@index')->name('home');

// Rotas Adm
Route::get('/gestao', 'GestaoController@index')->name('gestao');
Route::get('/gestao/vincular/{id}', 'GestaoController@vincular');
Route::post('/gestao/vincular/{id}/store', 'GestaoController@store');
Route::get('/gestao/create', 'GestaoController@create');
Route::post('/gestao/create/salvar', 'GestaoController@salvar');
Route::delete('/gestao/delete/{id}', 'GestaoController@delete')->name('gestao.empresa.delete');


Route::group(['prefix' => 'painel'], function() {
    // Dashboard
    Route::get('dashboard', 'Sistema\DashboardController@index')->name('dashboard.index');
    Route::get('dashboard/show', 'Sistema\DashboardController@show')->name('dashboard.show');
    // Empresa
    Route::get('empresa/{id}', 'Sistema\EmpresaController@edit')->name('empresa.edit');
    Route::patch('empresa/{id}/update', 'Sistema\EmpresaController@update')->name('empresa.update');
    
    //Perfil
    Route::get('perfil/edit', 'Sistema\PerfilController@edit')->name('perfil.edit');
    Route::patch('perfil/update', 'Sistema\PerfilController@update')->name('perfil.update');
    Route::get('perfil/reset', 'Sistema\PerfilController@reset')->name('perfil.reset');
    Route::patch('perfil/updatereset', 'Sistema\PerfilController@updatereset')->name('perfil.updatereset');
    
    //Usuário
    Route::get('usuario', 'Sistema\UsuarioController@index')->name('usuario.list');
    Route::get('usuario/create', 'Sistema\UsuarioController@create')->name('usuario.create');
    Route::post('usuario/store', 'Sistema\UsuarioController@store')->name('usuario.store');
    
    // Cliente
    Route::get('cliente', 'Sistema\ClienteController@index')->name('clientes.lista');
    Route::get('cliente/create', 'Sistema\ClienteController@create')->name('clientes');
    Route::post('cliente/store', 'Sistema\ClienteController@store')->name('cliente.store');
    Route::get('cliente/{id}/edit', 'Sistema\ClienteController@edit')->name('clientes.edit');;
    Route::patch('cliente/{id}/update', 'Sistema\ClienteController@update')->name('cliente.update');
    Route::get('cliente/{id}/menu', 'Sistema\ClienteController@menu')->name('cliente.menu');
    Route::delete('cliente/{id}/delete', 'Sistema\ClienteController@delete')->name('cliente.delete');

    Route::group(['prefix' => 'cliente'], function() {
        // Atendimento
        Route::get('{cliente_id}/menu/atendimento', 'Sistema\AtendimentoController@index')->name('atendimento.lista');
        Route::get('{cliente_id}/menu/atendimento/create', 'Sistema\AtendimentoController@create')->name('atendimento.create');
        Route::post('{cliente_id}/menu/atendimento/store', 'Sistema\AtendimentoController@store')->name('atendimento.store');
        Route::get('{cliente_id}/menu/atendimento/{id}/edit', 'Sistema\AtendimentoController@edit')->name('atendimento.edit');
        Route::patch('{cliente_id}/menu/atendimento/{id}/update', 'Sistema\AtendimentoController@update')->name('atendimento.update');
        Route::delete('{cliente_id}/menu/atendimento/{id}/delete', 'Sistema\AtendimentoController@delete')->name('atendimento.delete');

        //Agenda
        Route::get('{cliente_id}/menu/agenda', 'Sistema\AgendaController@index')->name('agenda.list');
        Route::get('{cliente_id}/menu/agenda/create', 'Sistema\AgendaController@create')->name('agenda.create');
        Route::post('{cliente_id}/menu/agenda/store', 'Sistema\AgendaController@store')->name('agenda.store');
        Route::get('{cliente_id}/menu/agenda/{id}/edit', 'Sistema\AgendaController@edit')->name('agenda.edit');
        Route::patch('{cliente_id}/menu/agenda/{id}/update', 'Sistema\AgendaController@update')->name('agenda.update');
        Route::delete('{cliente_id}/menu/agenda/{id}/delete', 'Sistema\AgendaController@delete')->name('agenda.delete');
    });

    
});

